const express = require('express')
const db = require('../db')
//const crypto = require('crypto-js')
const utils = require('../util')

const router = express.Router()

router.post('/admin', (request, response) => {
const { email, password } = request.body

const statement = `select id, name, password, mobile, address, email, role from Pizza_User where email = '${email}' and password = '${password}'`
db.connection.query(statement, (error, users) => {
const result = {}

if (error) {
result['status'] = 'error'
result['error'] = error
} else {

if (users.length == 0) {

result['status'] = 'error'
result['error'] = 'invalid credentials'
} else {

const user = users[0]

result['status'] = 'success'
result['data'] = user
}
}
response.send(result)
})
})

router.get('/', (request, response) => {
const statement = `select * from PIZZA_ITEMS`
db.connection.query(statement, (error, categories) => {
const result = utils.createResult(error, categories)
response.send(result)
})
})

router.post('/', (request, response) => {
const { name, type, category, description, price, image } = request.body
const statement = `insert into PIZZA_ITEMS ( name, type, category, description, price, image) values ('${name}','${type}','${category}','${description}','${price}','${image}')`
db.connection.query(statement, (error, categories) => {
const result = utils.createResult(error, categories)
response.send(result)
})
})

router.put('/:id', (request, response) => {
const { name, type, category, description, price, image} = request.body
const id = request.params.id
const statement = `update PIZZA_ITEMS set name= '${name}', type='${type}', category='${category}', description='${description}', price ='${price}', image='${image}' where id = ${id}`
db.connection.query(statement, (error, categories) => {
const result = utils.createResult(error, categories)
response.send(result)
})
})

router.delete('/:id', (request, response) => {
const id = request.params.id
const statement = `delete from PIZZA_ITEMS where id = ${id}`
db.connection.query(statement, (error, categories) => {
const result = utils.createResult(error, categories)
response.send(result)
})
})


module.exports = router 