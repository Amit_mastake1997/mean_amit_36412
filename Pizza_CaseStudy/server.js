const express = require('express')
const bodyParser = require('body-parser')

const routeradmin = require('./routes/admin')
const routercustomer = require('./routes/customer')


const app = express()
app.use(bodyParser.json())

app.use('/login',routeradmin)
app.use('/customer',routercustomer)

app.listen(4000, '0.0.0.0', () => {
console.log('server started on port 4000')
}) 