const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/:userId', (request, response) => {
  const {userId} = request.params
  const {title, contents} = request.body

  const statement = `insert into notes (title, contents, userId) values 
      ('${title}', '${contents}', '${userId}')`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/:userId', (request, response) => {
  const {userId} = request.params
  const statement = `select * from notes where userId = ${userId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/:noteId', (request, response) => {
  const {noteId} = request.params
  const {title, contents} = request.body

  const statement = `update notes set title = '${title}', contents = '${contents}' where id = ${noteId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/single/:noteId', (request, response) => {
  const {noteId} = request.params
  const statement = `delete from notes where id = ${noteId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/all/:userId', (request, response) => {
  const {userId} = request.params
  const statement = `delete from notes where userId = ${userId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router