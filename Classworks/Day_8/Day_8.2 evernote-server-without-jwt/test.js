const cryptoJs = require('crypto-js')
const secret = "sdnm234sadasd@#$@#ada084224323439sfkandsfn23489239484"

/// authenticated user

const data = {userId: 1}
const clientHash = `${cryptoJs.SHA256(JSON.stringify(data) + secret)}`
const userData = {data: data, hash: clientHash}
const strUserData = JSON.stringify(userData)

console.log(`user data = ${strUserData}`)

/// authenticated user





/// hacker user

const strHackerData = '{"data":{"userId":1},"hash":"2d413174c1b92a6fa971dab8fc44164d1a9b6d812dd3f1d9c999b84d1a747416"}'
const hackerData = JSON.parse(strHackerData)
hackerData['data']['userId'] = 2
hackerData['hash'] = `${cryptoJs.SHA256(JSON.stringify(hackerData['data']))}`

const modifiedData = JSON.stringify(hackerData)
console.log(`hacker has modifed the request: ${modifiedData}`)

/// hacker user






/// server 

const strServerData = '{"data":{"userId":2},"hash":"d447c5b7cab10e176c8f4f5b5efd95d23295581d071181ccd37dece62f98ff9a"}'
console.log(`server received: ${strServerData}`)

const serverData = JSON.parse(strServerData)
const serverHash = cryptoJs.SHA256(JSON.stringify(serverData['data']) + secret)
console.log(`server Hash = ${serverHash}`)
console.log(`client hash = ${serverData['hash']}`)

if (serverHash == serverData['hash']) {
  console.log(`delete from note where userId = ${serverData['data']['userId']}`)
} else {
  console.log('ahhh.. the request has been modified in between')
}


/// hacker 
