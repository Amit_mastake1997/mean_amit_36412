const express = require('express')
const db = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')

const router = express.Router()

router.post('/', (request, response) => {
  const {title, contents} = request.body

  try {
    const token = request.headers['token']
    const data = jwt.verify(token, '123n23w23sdfs09sdijf8h232j3324')

    const statement = `insert into notes (title, contents, userId) values 
        ('${title}', '${contents}', '${data.id}')`
    db.query(statement, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  } catch(ex) {
    response.status(401)
    response.send(utils.createResult('invalid token'))
  }
})

router.get('/', (request, response) => {
  try {
    const token = request.headers['token']
    const data = jwt.verify(token, '123n23w23sdfs09sdijf8h232j3324')

    const statement = `select * from notes where userId = ${data.id}`
    db.query(statement, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  } catch (ex) {
    response.status(401)
    response.send(utils.createResult('invalid token'))
  }
})

router.put('/:noteId', (request, response) => {
  const {noteId} = request.params
  const {title, contents} = request.body

  const statement = `update notes set title = '${title}', contents = '${contents}' where id = ${noteId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/single/:noteId', (request, response) => {
  const {noteId} = request.params
  const statement = `delete from notes where id = ${noteId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/all/:userId', (request, response) => {
  const {userId} = request.params
  const statement = `delete from notes where userId = ${userId}`
  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router