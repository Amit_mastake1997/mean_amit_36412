const express = require('express')
const bodyParser = require('body-parser')

// get routes
const userRotuer = require('./routes/user')
const noteRotuer = require('./routes/note')

const app = express()
app.use(bodyParser.json())

// add the routes
app.use('/user', userRotuer)
app.use('/note', noteRotuer)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})