const express = require('express')
const db = require('../db')
const utils = require('../utils')
const cryptoJs = require('crypto-js')
const uuid = require('uuid')
const mailer = require('../mailer')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()

router.post('/signup', (request, response) => {
  const {name, email, phone, address, password} = request.body
  const encryptedPassword = cryptoJs.SHA256(password)

  // create uuid for user activation
  const activationToken = uuid.v1()

  const statement = `insert into user(name, phone, address, email, password, activationToken) values 
      ('${name}', '${phone}','${address}','${email}','${encryptedPassword}', '${activationToken}')`
  db.query(statement, (error, result) => {

    // send activation link by email 
    const body = `
      <h1>welcome to the evernote application</h1>
      <div>
        please activate your account here 
        <div>
          <a href="http://localhost:4000/user/activate/${activationToken}">activate my account</a>
        </div>
      </div>
    `
    mailer.sendEmail(email, 'Activate your account', body, (mailerError, mailerResult) => {
      response.send(utils.createResult(error, result))
    })
  })
})

router.post('/signin', (request, response) => {
  const {email, password} = request.body
  const encryptedPassword = cryptoJs.SHA256(password)
  const statement = `select id, name, phone, address, active from user 
        where email = '${email}' and password = '${encryptedPassword}'`
  db.query(statement, (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      if (users.length == 0) {
        result['status'] = 'error'
        result['error'] = 'user not found'
      } else {
        const user = users[0]

        // check if user is active user
        if (user['active'] == 0) {
          // user account is not active
          result['status'] = 'error'
          result['data'] = 'your account is not active. please activate your account by using the link you have received in your registration confirmation email.'
        } else if (user['active'] == 1) {

          // create the token
          const authToken = jwt.sign({id: user['id']}, config.secret)

          result['status'] = 'success'
          result['data'] = {
            name: user['name'],
            address: user['address'],
            phone: user['phone'],
            authtoken: authToken
          }
        }
      }
    }

    response.send(result)
  })
})

router.get('/activate/:token', (request, response) => {
  const {token} = request.params
  const statement = `update user set active = 1 where activationToken = '${token}'`
  db.query(statement, (error, data) => {
    let body = ''
    if (error) {
      body = `
        <h1>Error occurred whiel activating your account..</h1>
        <h5>${error}</h5>
      `
    } else {
      body = `
        <h1>Congratulations!!! Successfully activated your account.</h1>
        <h5>Please login to continue</h5>
      `
    }

    //TODO: delete the activationToken

    response.send(body)
  })
})

router.get('/profile', (request, response) => {
  const statement = `select id, name, email, phone, address from user where id = ${request.userId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

module.exports = router