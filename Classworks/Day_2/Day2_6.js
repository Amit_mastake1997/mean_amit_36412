// create an object using Object
// in JS, 
// - Object is a root function which is used to 
//   create an object
// - object is an instance

// create a new instance of Object
const person1 = new Object()

// add or update properties
person1.name = "person1"
person1.address = "pune"
person1.age = 10

// console.log(person1)

// get property values
console.log(`name: ${person1.name}`)
console.log(`address: ${person1.address}`)
console.log(`age: ${person1.age}`)

console.log('--------------------')

const person2 = new Object()

// add or update properties
person2['name'] = 'person2'
person2['address'] = 'mumbai'
person2['age'] = 20

// console.log(person2)

// get property values
console.log(`name: ${person2['name']}`)
console.log(`address: ${person2['address']}`)
console.log(`age: ${person2['age']}`)

console.log('--------------------')

// create a new instance of Object
const person3 = new Object()

// add or update properties
person3.fullName = "person3"
person3['permanentAddress'] = "karad"
person3.currentAge = 30
person3.mobile = "+91234234234"
person3.email = "person3@test.co,"

console.log(person3)

// get property values
// console.log(`name: ${person3['name']}`)
// console.log(`address: ${person3.address}`)
// console.log(`age: ${person3['age']}`)

console.log('--------------------')


const mobile1 = new Object()

mobile1.model = 'iPhone XS Max'
mobile1.company = 'Apple'
mobile1.price = 144000

console.log(mobile1)