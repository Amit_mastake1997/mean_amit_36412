
// object
// - collection of properties and methods
// - the following object has properties
//   - name, address, age
// - the following object has methods
//   - canVote, printInfo
const person1 = {

    // properties
    name: 'person1', 
    address: 'pune', 
    age: 20, 

    // method
    canVote: function() {
      if (this['age'] >= 18) {
        console.log('person is eligible for voting')
      } else {
        console.log('person is not eligible for voting')
      }
    },

    // method
    printInfo: function() {
      console.log(`name: ${this.name}`)
      console.log(`address: ${this.address}`)
      console.log(`age: ${this.age}`)
    }    
}

// get the value of a property
// console.log(`name: ${person1.name}`)
// console.log(`address: ${person1.address}`)
// console.log(`age: ${person1.age}`)

// console.log(`name: ${person1['name']}`)
// console.log(`address: ${person1['address']}`)
// console.log(`age: ${person1['age']}`)

// canVote is a method
person1.canVote()
person1.printInfo()


const person2 = {

  // properties
  name: 'person2',
  address: 'mumbai',
  age: 10,

  // method
  canVote: function() {
    if (this['age'] >= 18) {
      console.log('person is eligible for voting')
    } else {
      console.log('person is not eligible for voting')
    }
  },

  // method
  printInfo: function() {
    console.log(`name: ${this.name}`)
    console.log(`address: ${this.address}`)
    console.log(`age: ${this.age}`)
  }  
}


// OOP
person2.canVote()
person2.printInfo()