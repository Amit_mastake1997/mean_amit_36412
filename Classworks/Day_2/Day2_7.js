// create object using constructor function

// constructor function
function Person(name, address, age) {
  this.name = name
  this.address = address
  this.age = age
}

// const p1 = new Person('person1', 'pune', 10)
// console.log(p1)

// const p2 = new Person('person2', 'mumbai', 20)
// console.log(p2)

// const p3 = new Person('person3', 'karad', 30)
// console.log(p3)


// mobile: model, company, price


// constructor function for mobile
function Mobile(model, company, price) {
  this.model = model
  this.company = company
  this.price = price
}

const m1 = new Mobile('iPhone XS Max', 'Apple', 144000)
console.log(m1)

const m2 = new Mobile('Note Pro', 'Xiomi', 15000)
console.log(m2)
