function canVote() {
  if (this['age'] >= 18) {
    console.log('person is eligible for voting')
  } else {
    console.log('person is not eligible for voting')
  }
}

function printInfo() {
  console.log(`name: ${this.name}`)
  console.log(`address: ${this.address}`)
  console.log(`age: ${this.age}`)
}  

const person1 = {

  // properties
  name: 'person1', 
  address: 'pune', 
  age: 20, 

  // method
  canVote: canVote,
  printInfo: printInfo
}

person1.canVote()
person1.printInfo()

const person2 = {

  // properties
  name: 'person2',
  address: 'mumbai',
  age: 10,

  // method
  canVote: canVote,
  printInfo: printInfo
}

person2.canVote()
person2.printInfo()
