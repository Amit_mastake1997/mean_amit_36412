// in JS, object can be created by using one of the following ways
// - using JSON
// - using Object
// - using Constructor Function
// - using class

function canVote(person) {
  if (person['age'] >= 18) {
    console.log('person is eligible for voting')
  } else {
    console.log('person is not eligible for voting')
  }
}

function function1() {
  // object using JSON
  const person1 = {name: 'person1', address: 'pune', age: 20}
  console.log(person1)
  console.log(`type of person1 = ${typeof(person1)}`)

  canVote(person1)
  person1.canVote()

  const person2 = {name: 'person2', address: 'mumbai', age: 10}
  console.log(person2)
  console.log(`type of person2 = ${typeof(person2)}`)

  // POP
  canVote(person2)

  // person2.canVote()

}

function1()