import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []
  service: ProductService

  // dependency injection (DI)
  // - cerate an object of dependency automatically
  // - it uses singleton design pattern
  // - ProductListComponent is dependent on ProductService class
  constructor(service: ProductService) {
    this.service = service
  }

  ngOnInit(): void {
    // component object got created successfully

    const observable = this.service.getProducts()

    // when the response is received, consume it
    observable.subscribe(response => {
      console.log(response)
      
      // get the product list only when the status is success
      if (response['status'] == 'success') {
        this.products = response['data']
      }
    })
  }

}
