const express = require('express')

// get the router
const router = express.Router()

router.get('/order', (request, response) => {
  response.end('this is a GET /order')
})

module.exports = router