const express = require('express')

// used for routing 
const router = express.Router()

router.get('/user', (request, response) => {
  response.end('this is a GET /user')
})

// export the router
module.exports = router