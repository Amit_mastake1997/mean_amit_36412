const express = require('express')

// get the router object
const router = express.Router()

router.get('/product', (request, response) => {
  response.end('this is a GET /product')
})

// export the router
module.exports = router