const express = require('express')

// routers
const userRouter = require('./routes/user')
const productRouter = require('./routes/product')
const orderRouter = require('./routes/order')

// create an express instance
const app = express()

app.get('/', (request, response) => {
  response.end('this is a GET /')
})

// add the router
app.use(userRouter)
app.use(productRouter)
app.use(orderRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})