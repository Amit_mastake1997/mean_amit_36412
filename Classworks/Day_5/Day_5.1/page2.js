// import express
const express = require('express')

// instantiate express application
const app = express()

// route
// - combination of 
//   - http method
//   - request url

// GET /
app.get('/', (request, response) => {
  response.end('this is GET /')
})

// POST /
app.post('/', (request, response) => {
  response.end('this is POST /')
})

// PUT /
app.put('/', (request, response) => {
  response.end('this is PUT /')
})

// DELETE /
app.delete('/', (request, response) => {
  response.end('this is DELETE /')
})

// routes for product

// GET /product
app.get('/product', (request, response) => {
  console.log('select * from product')
  response.end('this is GET /')
})

app.post('/product', (request, response) => {
  console.log('insert into product')
  response.end('this is POST /')
})

app.put('/product', (request, response) => {
  console.log('update product')
  response.end('this is PUT /')
})

app.delete('/product', (request, response) => {
  console.log('delete from product')
  response.end('this is DELETE /')
})

// routes for category

app.get('/category', (request, response) => {
  console.log('select * from category')
  response.end('this is GET /')
})

app.post('/category', (request, response) => {
  console.log('insert into category')
  response.end('this is POST /')
})

app.put('/category', (request, response) => {
  console.log('update category')
  response.end('this is PUT /')
})

app.delete('/category', (request, response) => {
  console.log('delete from category')
  response.end('this is DELETE /')
})

// routes for order

app.get('/order', (request, response) => {
  console.log('select * from order')
  response.end('this is GET /')
})

app.post('/order', (request, response) => {
  console.log('insert into order')
  response.end('this is POST /')
})

app.put('/order', (request, response) => {
  console.log('update order')
  response.end('this is PUT /')
})

app.delete('/order', (request, response) => {
  console.log('delete from order')
  response.end('this is DELETE /')
})

// make the application listen on port 4000
app.listen(4000, '0.0.0.0', () => {
  console.log('application is listening on port 4000')
})
