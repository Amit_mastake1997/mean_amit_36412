const http = require('http')

const server = http.createServer((request, response) => {

  if (request.url == '/') {
    if (request.method == 'GET') {
      console.log('this is a test GET /')
    }
  } else if (request.url == '/user') {

    if (request.method == 'GET') {
      console.log('select * from user')
    } else if (request.method == 'POST') {
      console.log('insert into user (...)')
    } else if (request.method == 'PUT') {
      console.log('update user set ...')
    } else if (request.method == 'DELETE') {
      console.log('delete from user')
    }

  } else if (request.url == '/product') {

    if (request.method == 'GET') {
      console.log('select * from product')
    } else if (request.method == 'POST') {
      console.log('insert into product (...)')
    } else if (request.method == 'PUT') {
      console.log('update product set ...')
    } else if (request.method == 'DELETE') {
      console.log('delete from product')
    }

  } else if (request.url == '/category') {
    
    if (request.method == 'GET') {
      console.log('select * from category')
    } else if (request.method == 'POST') {
      console.log('insert into category (...)')
    } else if (request.method == 'PUT') {
      console.log('update category set ...')
    } else if (request.method == 'DELETE') {
      console.log('delete from category')
    }

  }

  // send response to the client
  response.end('data from server')
})

server.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})