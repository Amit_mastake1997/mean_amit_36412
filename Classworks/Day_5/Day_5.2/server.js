const express = require('express')

const app = express()

// middleware
function logRequest(request, response, next) {
  // logs
  console.log('inside the logRequest function')
  console.log(`method = ${request.method}`)
  console.log(`url = ${request.url}`)

  // call the next function
  next()
}

// call the logRequest method before calling the route function
app.use(logRequest)

app.get('/', (request, response) => {
  // logRequest(request, response)
  console.log('inside the GET /')
  console.log('this is a test GET /')
  response.end()
})

app.post('/', (request, response) => {
  // logRequest(request, response)
  console.log('inside the POST /')
  console.log('this is a test POST /')
  response.end()
})

app.put('/', (request, response) => {
  // logRequest(request, response)
  console.log('inside the PUT /')
  console.log('this is a test PUT /')
  response.end()
})

app.delete('/', (request, response) => {
  // logRequest(request, response)
  console.log('inside the DELETE /')
  console.log('this is a test DELETE /')
  response.end()
})

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})