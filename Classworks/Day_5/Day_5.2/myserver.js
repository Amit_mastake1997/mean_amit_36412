const express = require('express')

const app = express()

// middleware 
function log1(request, response, next) {
  console.log('inside log1')

  // call the next logical function
  next()
}

function log2(request, response, next) {
  console.log('inside log2')
  
  // call the next logical function
  next()
}


// anonymous function
const log3 = function(request, response, next) {
  console.log('inside log3')
  next()
}

// arrow function
const log4 = (request, response, next) => {
  console.log('inside log4')
  next()
}

// use the middleware 
app.use(log1)
app.use(log2)
app.use(log3)
app.use(log4)
app.use((request, response, next) => {
  console.log('inside anonymous arrow function')
  next()
})

app.get('/', (request, response) => {
  console.log('inside the route function')
  response.end('test GET /')
})

app.get('/product', (request, response) => {
  
  const products = [
    { id: 1, title: 'product 1', price: 100 },
    { id: 2, title: 'product 2', price: 200 },
    { id: 3, title: 'product 3', price: 300 }
  ]

  // change the content type
  response.setHeader('Content-type', 'application/json')

  // convert json object to a json string
  const strProducts = JSON.stringify(products)
  console.log(`type of strProducts = ${typeof(strProducts)}`)

  // send the product list to the client
  response.end(strProducts)
})


app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})