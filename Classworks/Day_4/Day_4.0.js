// import the file system module
const fs = require('fs')

function readSynchronous() {
  console.log('reading file started')

  try {
    // blocking API
    // - next statement will get blocked till the end of this API
    const data = fs.readFileSync('./myfile_1.txt')
    console.log('reading file finished')
    console.log(`data = ${data}`)
  } catch(ex) {
    console.log(`exception handled: ${ex}`)
  }

  console.log('started calculating multiplication')
  const multiplication = 23423345 * 435343545
  console.log('finish calculating multiplication')
  console.log(`multiplication = ${multiplication}`)
}

// readSynchronous()

function readAsynchronous() {
  console.log('read file started')

  // non-blocking API
  // async -> starts in a separt thread
  // - the arrow fucntion (2nd parameter) here is called as callback function
  // - callback function:
  //   - written by one and getting called by another one
  fs.readFile('./myfile_1.txt', (error, data) => {
    console.log('arrow function called')
    
    if (error) {
      // if there is any error while reading the contents of the file
      console.log(`there is an error: ${error}`)
    } else {
      // there is no error while reading the file
      console.log('reading file finished')
      console.log(`data = ${data}`)
    }
  })

  console.log('started calculating multiplication')
  const multiplication = 23423345 * 435343545
  console.log('finish calculating multiplication')
  console.log(`multiplication = ${multiplication}`)
}

readAsynchronous()