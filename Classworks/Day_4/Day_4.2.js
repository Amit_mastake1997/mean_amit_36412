// import the http package
const http = require('http')

// create server instance
// which will run continuously in the background
const server = http.createServer((request, response) => {
  console.log('a request is received..')

  // send the response to the client
  response.end()
})

// start the server listening on a 4000 port
// port number
// - 1-1024: reserved ports (http(80), https(443))
// - 1025 - 65534: for user applications
//   - mysql: 3306
//   - mongo: 27017
server.listen(4000, '0.0.0.0', () => {
  console.log('server started successfully on port 4000')
})