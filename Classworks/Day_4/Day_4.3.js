const http = require('http')

const server = http.createServer((request, response) => {
  console.log('request received')

  // add data into the response
  // send the response
  // response.end('hello from server')

  // change the content type to match the response data

  // response.setHeader('Content-Type', 'text/plain')

  // response.setHeader('Content-Type', 'text/html')
  // response.end('<h1 style="color: red">hello from server</h1>')

  response.setHeader('Content-Type', 'application/json')
  response.end('{"name": "person1", "age": 49, "address": "pune"}')

})

server.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})