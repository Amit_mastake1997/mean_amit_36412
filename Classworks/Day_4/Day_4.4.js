const http = require('http')

const server = http.createServer((request, response) => {
  console.log('received a request')

  // get the request params
  console.log(`url: ${request.url}`)
  console.log(`method: ${request.method}`)

  if (request.url == '/user') {
    if (request.method == 'GET') {
      console.log('select * from user')
    } else if (request.method == 'POST') {
      console.log('insert into user ...')
    } else if (request.method == 'PUT') {
      console.log('update user')
    } else if (request.method == 'DELETE') {
      console.log('delete from user')
    }
  } else if (request.url == '/product') {
    if (request.method == 'GET') {
      console.log('select * from product')
    } else if (request.method == 'POST') {
      console.log('insert into product ...')
    } else if (request.method == 'PUT') {
      console.log('update product')
    } else if (request.method == 'DELETE') {
      console.log('delete from product')
    }
  }

  console.log('------------------------------------------------')
  response.end()
})

server.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})