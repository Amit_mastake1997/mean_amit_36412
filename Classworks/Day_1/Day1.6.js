// number
let num = 100
// console.log(`num = ${num}, type = ${typeof(num)}`)

let num2 = num
// console.log(`num2 = ${num2}, type = ${typeof(num2)}`)

num = 300
// console.log(`num = ${num}, type = ${typeof(num)}`)
// console.log(`num2 = ${num2}, type = ${typeof(num2)}`)

// function
function f1() {
  console.log('inside f1')
}

// const f1 = function() {
//   console.log('inside f1')
// }
// console.log(`f1 = ${f1}, type = ${typeof(f1)}`)

// function alias
const myF1 = f1

// calling the function using original name
// f1()

// calling the function using function alias
// myF1()

function function2() {
  console.log('inside function2')
}

// function alias
const myFunction2 = function2
// function2()
// myFunction2()

// function alias
// anonymous function
const myFunction3 = function() {
  console.log('inside function3')
}

// myFunction3()
// anonymous function
// big fat arrow function
const myFunction4 = () => {
  console.log('inside function4')
}

// myFunction4()


// named function
// function square(number) {
//   console.log(`square of ${number} = ${number * number}`)
// }

// anonymous function
// const square = function(number) {
//   console.log(`square of ${number} = ${number * number}`)
// }

// arrow function
// const square = (number) => {
//   console.log(`square of ${number} = ${number * number}`)
// }

// square of 5 = 25
// square(5)


// arrow function
const add = (p1, p2) => {
  console.log(`addition = ${p1 + p2}`)
}

add(10, 30)