// function declaration
// parameterless function
function function1() {
  console.log('inside function1')
}

// function call
// function1()


// parameterized function
function function2(p1) {
  console.log('inside function2')
  console.log(`p1 = ${p1}, type = ${typeof(p1)}`)
}

// function2(10)
// function2('test')
// function2(true)
// function2()


function add1(p1, p2) {
  console.log('inside add')
  const addition = p1 + p2
  console.log(`addition = ${addition}`)
}

// add1(10, 30)
// add1(10, '30')


function function3(p1) {
  console.log('inside function3')
  console.log(`p1 = ${p1}, type = ${typeof(p1)}`)

  console.log(`arguments = ${arguments}`)
  console.log(arguments)
}

// function3()
// function3(10)

// * discarded 20
// function3(10, 20, 30, 40, 50)


// variable length argument function
function add() {
  console.log(arguments)
  let sum = 0
  for (const number of arguments) {
    sum += number
  }

  console.log(`addition = ${sum}`)
}

add(10)
add(10, 20)
add(10, 20, 30)
add(10, 20, 30, 40)
add(10, 20, 30, 40, 50)
add(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)