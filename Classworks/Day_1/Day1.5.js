// array (collection)
// - collection of values
// - mutable

function function1() {
  // array of numbers
  const numbers = [10, 20, 30, 40, 50]
  console.log(numbers)
  console.log(`type = ${typeof(numbers)}`)

  // iterate over the array
  // traditional for loop
  for (let index = 0; index < numbers.length; index++) {
    console.log(`value at ${index} = ${numbers[index]}`)
  }

   console.log('')

  // foreach loop
  for (let number of numbers) {
    console.log(`number = ${number}`)
  }

}

// function1()

function function2() {
  // array of strings
  const countries = ["india", 'USA', 'UK']
  console.log(countries)
  console.log(`type = ${typeof(countries)}`)

  // foreach 
  for (const country  of countries) {
    console.log(`country = ${country}`)
  }
}

// function2()


function function3() {
  const numbers = [10, 20, 30, 40, 50]
  console.log(numbers)

  // append a value to the end of the collection
  numbers.push(60)
  console.log(numbers)
  numbers.push(70)
  console.log(numbers)

  // delete the last value
  const lastValue = numbers.pop()
  console.log(`deleted value = ${lastValue}`)
  console.log(numbers)

  // delete a value by using index position
  numbers.splice(3, 2)
  console.log(numbers)
}


// function3()


function function4() {
  const countries = ['india', 'usa', 'uk']
  console.log(countries)

  // appending a value at the end
  countries.push('china')
  console.log(countries)

  // remove the last value
  countries.pop()
  console.log(countries)
}

function4()