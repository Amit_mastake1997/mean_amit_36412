// number
const num = 100
console.log(`num = ${num}, type = ${typeof(num)}`)

// number
const salary = 10.50
console.log(`salary = ${salary}, type = ${typeof(salary)}`)

// string
const firstName = 'steve'
console.log(`firstName = ${firstName}, type = ${typeof(firstName)}`)

// string
const lastName = "jobs"
console.log(`lastName = ${lastName}, type = ${typeof(lastName)}`)

// string
const address = `
building name,
city,
state,
country,
zipCode`
console.log(`address = ${address}, type = ${typeof(address)}`)

// boolean
const canVote = true
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

// undefined
let myvar
console.log(`myvar = ${myvar}, type = ${typeof(myvar)}`)

// 
const person = { name: "person1", address: "pune", phone: "+9123243" }
console.log(`person = ${person}, type = ${typeof(person)}`)
console.log(person)