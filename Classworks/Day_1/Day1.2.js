// variables
let num = 100
console.log(`num = ${num}`)
num = 200
console.log(`num = ${num}`)

// constants
// always prefer using constant
const pi = 3.14
console.log(`pi = ${pi}`)

// can not update the value
// pi = 7.22
// console.log(`pi = ${pi}`)