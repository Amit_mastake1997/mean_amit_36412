const { request } = require('express')
const express = require('express')

// import mysql js driver
const mysql = require('mysql')

const router = express.Router()

router.get('/product', (request, response) => {
  // step 1: open the database connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mystore',
    port: 3306
  })

  // step 2: create query statement
  const statement = 'select id, title, description, price from product'

  // step 3: execute the query
  connection.query(statement, (error, data) => {     

    // step 4: process the data
    if (error) {
      // there is an error while executing the query
      console.error(`error: ${error}`)
    } else {
      // no error - query got executed successfully
      console.log(data)
    }

    // step 5: close the connection
    connection.end()

    response.send(data)
  })

})

router.post('/product', (request, response) => {

  // get the input from client
  const title = request.body.title
  const description = request.body.description
  const price = request.body.price

  // open the connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mystore',
    port: 3306
  })

  // prepare the query statement
  const statement = `insert into product (title, description, price) values ('${title}', '${description}', '${price}')`

  // execute the statement
  connection.query(statement, (error, result) => {
    // process the result
    if (error) {
      console.error(`error: ${error}`)
    }

    // close the connection
    connection.end()

    response.send(result)
  })
  
})

router.put('/product/:id', (request, response) => {
  const id = request.params.id
  const price = request.body.price
  const title = request.body.title
  const description = request.body.description

  // open connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mystore',
    port: '3306'
  })

  // prepare statement
  const statement = `update product set title = '${title}', description = '${description}', price = ${price} where id = ${id}`

  // execute stsatement
  connection.query(statement, (error, result) => {
    // process result
    if (error) {
      console.error(`error: ${error}`)
    }

    // close the connection
    connection.end()
    
    response.send(result)
  })

})

router.delete('/product/:id', (request, response) => {
  const id = request.params.id

  // open connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mystore',
    port: 3306
  })

  // prepare statement
  const statement = `delete from product where id = ${id}`

  // execute query
  connection.query(statement, (error, result) => {
    // process result
    if (error) {
      console.error(`error: ${error}`)
    }

    // close the connection  
    connection.end()

    response.send(result)
  })
  
})

module.exports = router