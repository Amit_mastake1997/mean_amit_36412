const express = require('express')
const bodyParser = require('body-parser')

// import all routes
const routerProduct = require('./routes/product')

const app = express()

// for request body
app.use(bodyParser.json())

// add router to the application server
app.use(routerProduct)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})