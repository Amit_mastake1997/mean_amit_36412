const express = require('express')

const router = express.Router()

const products = [
  { id: 1, title: 'product1', description: 'test description 1', price: 100 }
]

// create
router.post('/product', (request, response) => {
  const body = request.body
  
  // read all the values from request body
  const id = body.id
  const title = body.title
  const description = body.description
  const price = body.price

  // insert a new object into the products array
  products.push({
    id: id,
    title: title,
    description: description,
    price: price
  })
  response.send('product added')
})

// read
router.get('/product', (request, response) => {
  // const strProducts = JSON.stringify(products)
  // response.setHeader('Content-Type', 'application/json')
  // response.end(strProducts)

  response.send(products)
})

// update
// router.put('/product', (request, response) => {
//   const body = request.body

//   // the product to modify
//   const id = body.productId

//   // new price
//   const price = body.price

//   for (let index = 0; index < products.length; index++) {
//     const product = products[index];
//     if (product['id'] == id) {
//       // found the product to update

//       // update the product price
//       product['price'] = price
//     }
//   }

//   // update product set price = <new price> where id = <product id>;

//   response.send('updated product')
// })


// accept the product id using query parameter
router.put('/product/:id', (request, response) => {
  const body = request.body

  // the product to modify
  console.log(request.params)
  const id = request.params.id

  // new price
  const price = body.price

  for (let index = 0; index < products.length; index++) {
    const product = products[index];
    if (product['id'] == id) {
      // found the product to update

      // update the product price
      product['price'] = price
    }
  }

  // update product set price = <new price> where id = <product id>;

  response.send('updated product')
})

// delete
router.delete('/product/:id', (request, response) => {
  const id = request.params.id
  for (let index = 0; index < products.length; index++) {
    const product = products[index];
    if (product.id == id) {
      // found the product
      products.splice(index, 1)
      break
    }
  }

  response.send('dleted product')
})

module.exports = router