const express = require('express')
const bodyParser = require('body-parser')

// import routes
const routerProducts = require('./routes/product')

const app = express()

// used to get the input parameters sent through request body
app.use(bodyParser.json())

// add the routes to the application
app.use(routerProducts)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})