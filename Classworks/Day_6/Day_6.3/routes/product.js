const express = require('express')
const db = require('../db')

const router = express.Router()

router.get('/product', (request, response) => {
  const connection = db.connection()
  const statement = `select id, title, description, price from product`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})

router.post('/product', (request, response) => {
  const title = request.body.title
  const price = request.body.price
  const description = request.body.description

  const connection = db.connection()
  const statement = `insert into product (title, price, description) values ('${title}', '${price}', '${description}')`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})

router.put('/product/:id', (request, response) => {
  const id = request.params.id
  const title = request.body.title
  const price = request.body.price
  const description = request.body.description

  const connection = db.connection()
  const statement = `update product set title = '${title}', price = '${price}', description = '${description}' where id = ${id}`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})

router.delete('/product/:id', (request, response) => {
  const id = request.params.id
  const connection = db.connection()
  const statement = `delete from product where id = ${id}`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})


module.exports = router