// import os module
// - custom module: requires path to start with ./ or ../ or path to the module
// - system or third party module: requires only the module name
const os = require('os')

// console.log(os)

console.log('--- cpu info ---')
const cpus = os.cpus()
for (const cpu of cpus) {
  console.log(`model: ${cpu.model}, speed: ${cpu.speed}`)
}
console.log('--- cpu info ---')
console.log('')

console.log('--- network interfaces ---')

const networkInterfaces = os.networkInterfaces()
console.log(networkInterfaces)

// for (const networkInterface of networkInterfaces) {
//   console.log(networkInterface)
// }

console.log('--- network interfaces ---')
console.log('')


console.log(`platform: ${os.platform()}`)
console.log(`OS release: ${os.release()}`)
console.log(`architecture: ${os.arch()}`)
console.log(`memory: ${os.freemem() / (1024 * 1024 * 1024)}GB`)
console.log(`memory: ${os.totalmem() / (1024 * 1024 * 1024)}GB`)

console.log(`home directory: ${os.homedir()}`)
console.log(`hostname: ${os.hostname()}`)


// alert("hello node")