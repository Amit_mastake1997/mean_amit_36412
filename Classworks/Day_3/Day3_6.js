function add(p1, p2) {
  console.log(`addition = ${p1 + p2}`)
}

function subtract(p1, p2) {
  console.log(`subtraction = ${p1 - p2}`)
}

function multiply(p1, p2) {
  console.log(`multiplication = ${p1 * p2}`)
}

function divide(p1, p2) {
  console.log(`division = ${p1 / p2}`)
}

// constants
const pi = 3.14

// add(10, 20)
// subtract(10, 5)
// divide(10, 2)
// multiply(4, 5)

// export add
// module.exports = add

// export subtract
// module.exports = subtract

// json object
// - collection of key-value pairs
// - 'add', 'subtract' are the keys
// - add, subtract values (refereces to functions)
module.exports = {
  'add': add,
  subtract: subtract,
  multiply: multiply,
  divide: divide,
  pi: pi
}

// node adds the module object at run time
// console.log(module)
