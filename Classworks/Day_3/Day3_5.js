// const num = 100
// console.log(`num = ${num}`)


// constructor function
function Person(name, address, age) {
  this.name = name
  this.address = address
  this.age = age
}

Person.prototype.toString = function() {
  return `Person {Name: ${this.name}, Address: ${this.address}, Age: ${this.age}}`
}

const p1 = new Person('person1', 'pune', 10)
console.log(`p1 = ${p1}`)

const p2 = new Person('person2', 'mumbai', 10)

// p2 has overriden the toString method
p2.toString = function() {
  return `Person {Name: ${this.name}, Address: ${this.address}}`
}

// cosnt strP2 = p2.toString()
console.log(`p2 = ${p2}`)

const p3 = new Person('person3', 'karad', 20)
console.log(`p3 = ${p3}`)