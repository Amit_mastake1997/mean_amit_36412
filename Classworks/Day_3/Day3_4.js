// Person
// - name, address, age
// - printInfo, canVote

// constructor function
function Person(name, address, age) {
  this.name = name
  this.address = address
  this.age = age
}

// method
Person.prototype.canVote = function() {
  if (this.age >= 18) {
    console.log(`${this.name} is eligible for voting`)
  } else {
    console.log(`${this.name} is not eligible for voting`)
  }
}

// method
Person.prototype.printInfo = function() {
  console.log(`name: ${this.name}`)
  console.log(`address: ${this.address}`)
  console.log(`age: ${this.age}`)
}


// create object
const p1 = new Person('person1', 'pune', 10)
const p2 = new Person('person2', 'mumbai', 20)

// p1.printInfo()
p1.canVote()

// p2.printInfo()
p2.canVote()

