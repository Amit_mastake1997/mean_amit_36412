// using JSON
const mobile = {
  model: "iphone",
  company: "Apple",
  price: 144000,

  printInfo: function() {
    console.log(`model : ${this.model}`)
    console.log(`company : ${this.company}`)
    console.log(`price : ${this.price}`)
  }
}

mobile.printInfo()
