function Mobile(model, company, price) {
  this.model = model
  this.company = company
  this.price = price
}

const m1 = new Mobile('iPhone', 'Apple', 144000)
m1.printInfo = function() {
  console.log(`model : ${this.model}`)
  console.log(`company : ${this.company}`)
  console.log(`price : ${this.price}`)
  console.log('------------------')
}

const m2 = new Mobile('Galaxy Note Pro', 'Samsung', 80000)
m2.printInfo = function() {
  console.log(`model : ${this.model}`)
  console.log(`company : ${this.company}`)
  console.log(`price : ${this.price}`)
  console.log('------------------')
}


m1.printInfo()
m2.printInfo()

console.log(m1)
console.log(m2)