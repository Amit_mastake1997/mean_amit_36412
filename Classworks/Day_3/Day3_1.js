const mobile = new Object()

// properties
mobile.model = "Galaxy Note Pro"
mobile.company = "Samsung"
mobile.price = 80000

// methods
mobile.printInfo = function() {
  console.log(`model : ${this.model}`)
  console.log(`company : ${this.company}`)
  console.log(`price : ${this.price}`)
}

mobile.printInfo()