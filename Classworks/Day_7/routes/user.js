const express = require('express')
const db = require('../db')
const crypto = require('crypto-js')
const utils = require('../utils')

const router = express.Router()

router.post('/signup', (request, response) => {
  // turn all the keys from request.body into constants
  const { firstName, lastName, email, password, address, phone } = request.body

  // encrypt the password
  const encryptedPassword = crypto.SHA256(password)

  const statement = `insert into user (firstName, lastName, email, password, address, phone) values (
    '${firstName}', '${lastName}', '${email}', '${encryptedPassword}', '${address}', '${phone}')`
  
  db.connnection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.post('/login', (request, response) => {
  const { email, password } = request.body

  // encrypt the password
  const encryptedPassword = crypto.SHA256(password)

  const statement = `select id, firstName, lastName, address, phone from user where email = '${email}' and password = '${encryptedPassword}'`
  db.connnection.query(statement, (error, users) => {
    const result = {}

    if (error) {
      // error while executing the query
      result['status'] = 'error'
      result['error'] = error
    } else {
      // check if any user has email and passowrd
      if (users.length == 0) {
        // there is no user matching email and password
        result['status'] = 'error'
        result['error'] = 'invalid credentials'
      } else {
        // get the logged in user's profile
        const user = users[0]

        // user found with matching email and password
        result['status'] = 'success'
        result['data'] = user
      }
    }

    response.send(result)
  })
})


module.exports = router