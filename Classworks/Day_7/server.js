const express = require('express')
const bodyParser = require('body-parser')

// routers
const routerUser = require('./routes/user')
const routerCategory = require('./routes/category')

const app = express()

// add json parser
app.use(bodyParser.json())

app.get('/', (request, response) => {
  response.send('<h1>Welcome to my backend</h1>')
})

// add the routers
app.use('/user', routerUser)
app.use('/category', routerCategory)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})