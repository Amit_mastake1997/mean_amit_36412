var Person = /** @class */ (function () {
    // constructor
    function Person() {
        console.log('inside constructor');
        this.name = '';
        this.age = 0;
        this.address = '';
    }
    Person.prototype.printInfo = function () {
        console.log("name: " + this.name);
        console.log("age: " + this.age);
        console.log("address: " + this.address);
    };
    return Person;
}());
// instantiation of Person class
var p1 = new Person();
// initializing the object
p1.name = 'person1';
p1.address = 'pune';
p1.age = 10;
// consume the object methods
p1.printInfo();
console.log(p1);
