class Person {
  name: string
  age: number
  address: string

  // constructor
  constructor() {
    console.log('inside constructor')
    this.name = ''
    this.age = 0
    this.address = ''
  }

  printInfo() {
    console.log(`name: ${this.name}`)
    console.log(`age: ${this.age}`)
    console.log(`address: ${this.address}`)
  }
}

// instantiation of Person class
const p1 = new Person()

// initializing the object
p1.name = 'person1'
p1.address = 'pune'
p1.age = 10

// consume the object methods
p1.printInfo()
console.log(p1)
