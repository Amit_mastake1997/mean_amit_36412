// variable
var age = 40;
// function
function canVote() {
}
// class
// - template to create an object
// - collection of properties and methods
var Person = /** @class */ (function () {
    function Person() {
    }
    // methods
    Person.prototype.canVote = function () {
        if (this.age >= 18) {
            console.log(this.name + " is eligible");
        }
        else {
            console.log(this.name + " is NOT eligible");
        }
    };
    Person.prototype.printInfo = function () {
        console.log("name : " + this.name);
        console.log("age : " + this.age);
        console.log("address : " + this.address);
    };
    return Person;
}());
// creating an object of class Person
var p1 = new Person();
// setting the properties
p1.name = 'person1';
p1.address = 'pune';
p1.age = 30;
// calling mtehods
p1.printInfo();
p1.canVote();
// create a class Computer: cpu, gpu, ram, price,  printInfo(), canAfford()
var Computer = /** @class */ (function () {
    function Computer() {
    }
    // methods
    Computer.prototype.printInfo = function () {
        console.log("cpu: " + this.cpu);
        console.log("gpu: " + this.gpu);
        console.log("ram: " + this.ram);
        console.log("price: " + this.price);
    };
    Computer.prototype.canAfford = function () {
        if (this.price <= 50000) {
            console.log("this computer is affordable");
        }
        else {
            console.log("this computer is NOT affordable");
        }
    };
    return Computer;
}());
var c1 = new Computer();
c1.cpu = "intel core i9";
c1.gpu = "AMD";
c1.ram = "16GB";
c1.price = 40000;
c1.printInfo();
c1.canAfford();
