// explicit variable declaration [with data type]
// c code
// int num = 10;
// number
var num = 10;
console.log("num = " + num + ", type = " + typeof (num));
// string
var str = 'typescript';
console.log("str = " + str + ", type = " + typeof (str));
// boolean 
var canVote = false;
console.log("canVote = " + canVote + ", type = " + typeof (canVote));
// undefined
var myVar = undefined;
console.log("myVar = " + myVar + ", type = " + typeof (myVar));
// object
var person1 = { name: "person1", age: 30 };
console.log("person1 = " + person1 + ", type = " + typeof (person1));
// object
var mobile = { model: 'iPhon 12 Pro Max', price: 156000 };
console.log("mobile = " + mobile + ", type = " + typeof (mobile));
