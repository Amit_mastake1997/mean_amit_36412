// implicit declaration
// the data type will be implicitly assigned

// number
let num = 10
console.log(`num = ${num}, type = ${typeof(num)}`)

// num can not be converted to another data type
// num = "test"
// num = true
// num = {name: "test"}

// number
const salary = 10.50
console.log(`salary = ${salary}, type = ${typeof(salary)}`)

// string
const str = "string1"
console.log(`str = ${str}, type = ${typeof(str)}`)

// boolean
const canVote = true
console.log(`canVote = ${canVote}, type = ${typeof(canVote)}`)

// object
const person = {name: 'person1', age: 40}
console.log(`person = ${person}, type = ${typeof(person)}`)

// undefined
let myvar
console.log(`myvar = ${myvar}, type = ${typeof(myvar)}`)
