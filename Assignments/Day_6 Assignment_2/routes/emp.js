
const express = require('express')

const db = require('../db')

const router = express.Router()

//select query
router.get('/emp', (request, response) => {
  const connection = db.connection()
  const statement = `select Emp_ID, Name, Email, Password,Mobile,Address,Salary,Designation from EMP_tbl`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})


//insert query

router.post('/emp', (request, response) => {
    const Name = request.body.Name
    const Email = request.body.Email
    const Password = request.body.Password
    const Mobile= request.body.Mobile
    const Address= request.body.Address
    const Salary= request.body.Salary
    const Designation= request.body.Designation
    
  
    const connection = db.connection()
    const statement = `insert into EMP_tbl (Name, Email, Password,Mobile,Address,Salary,Designation) values ('${Name}', '${Email}', '${Password}','${Mobile}','${Address}','${Salary}','${Designation}')`
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(result)
    })
  })


//update query
router.put('/emp/:Emp_ID', (request, response) => {
    const Emp_ID = request.params.Emp_ID
    const Name = request.body.Name
    const Email = request.body.Email
    const Password = request.body.Password
    const Mobile= request.body.Mobile
    const Address= request.body.Address
    const Salary= request.body.Salary
    const Designation= request.body.Designation
    
  
    const connection = db.connection()
    const statement = `update EMP_tbl set Name = '${Name}', Email = '${Email}', Password = '${Password}',Mobile = '${Mobile}', Address = '${Address}',Salary = '${Salary}', Designation = '${Designation}' where Emp_ID = ${Emp_ID}`
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(result)
    })
  })


//delete query

router.delete('/emp/:Emp_ID', (request, response) => {
    const Emp_ID = request.params.Emp_ID
    const connection = db.connection()
    const statement = `delete from EMP_tbl where Emp_ID = ${Emp_ID}`
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(result)
    })
  })
  

module.exports = router