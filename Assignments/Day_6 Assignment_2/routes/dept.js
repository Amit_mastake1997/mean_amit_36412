
const express = require('express')

const db = require('../db')

const router = express.Router()


//select query 
router.get('/dept', (request, response) => {
  const connection = db.connection()
  const statement = `select Dept_ID, Name, Location from Dept_tbl`
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})



//insert query
router.post('/dept', (request, response) => {
    const Name = request.body.Name
    const Location = request.body.Location
  
    const connection = db.connection()
    const statement = `insert into Dept_tbl (Name, Location) values ('${Name}', '${Location}')`
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(result)
    })
  })


//update query
router.put('/dept/:Dept_ID', (request, response) => {
    const Dept_ID = request.params.Dept_ID
    const Name = request.body.Name
    const Location = request.body.Location
  
    const connection = db.connection()
    const statement = `update Dept_tbl set Name = '${Name}', Location = '${Location}' where Dept_ID = ${Dept_ID}`
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(result)
    })
  })
  

//delete query
router.delete('/dept/:Dept_ID', (request, response) => {
    const Dept_ID = request.params.Dept_ID
    const connection = db.connection()
    const statement = `delete from Dept_tbl where Dept_ID = ${Dept_ID}`
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(result)
    })
  })

module.exports = router