const express = require('express')
const bodyParser = require('body-parser')

const routerDept = require('./routes/dept')
const routerEmp = require('./routes/emp')

const app = express()
app.use(bodyParser.json())

app.use(routerDept)
app.use(routerEmp)

app.listen(4008, '0.0.0.0', () => {
  console.log('server started on port 4008')
})