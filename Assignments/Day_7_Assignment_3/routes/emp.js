
const express = require('express')

const utils = require('../utils')

const db = require('../db')

const crypto = require('crypto-js')

const router = express.Router()

//select query
// router.get('/emp', (request, response) => {
//   const connection = db.connection()
//   const statement = `select Emp_ID, Name, Email, Password,Mobile,Address,Salary,Designation from EMP_tbl`
//   connection.query(statement, (error, result) => {
//     connection.end()
//     response.send(result)
//   })
// })


router.get('/', (request, response) => {
  const statement = `select Emp_ID, Name, Email, Password,Mobile,Address,Salary,Designation from EMP_tbl`
  db.connnection.query(statement, (error, emps) => {
    // create a result using status and data/error keys
    const result = utils.createResult(error, emps)
    response.send(result)
  })
})


//insert query

router.post('/', (request, response) => {
    const Name = request.body.Name
    const Email = request.body.Email
    const Password = request.body.Password
    const Mobile= request.body.Mobile
    const Address= request.body.Address
    const Salary= request.body.Salary
    const Designation= request.body.Designation
    
  
   
    const statement = `insert into EMP_tbl (Name, Email, Password,Mobile,Address,Salary,Designation) values ('${Name}', '${Email}', '${Password}','${Mobile}','${Address}','${Salary}','${Designation}')`
    db.connnection.query(statement, (error, emps) => {
      // create a result using status and data/error keys
      const result = utils.createResult(error, emps)
      response.send(result)
    })
  })


//update query
router.put('/:Emp_ID', (request, response) => {
    const Emp_ID = request.params.Emp_ID
    const Name = request.body.Name
    const Email = request.body.Email
    const Password = request.body.Password
    const Mobile= request.body.Mobile
    const Address= request.body.Address
    const Salary= request.body.Salary
    const Designation= request.body.Designation
    
  
  
    const statement = `update EMP_tbl set Name = '${Name}', Email = '${Email}', Password = '${Password}',Mobile = '${Mobile}', Address = '${Address}',Salary = '${Salary}', Designation = '${Designation}' where Emp_ID = ${Emp_ID}`
    db.connnection.query(statement, (error, emps) => {
      // create a result using status and data/error keys
      const result = utils.createResult(error, emps)
      response.send(result)
    })
  })


//delete query

router.delete('/:Emp_ID', (request, response) => {
    const Emp_ID = request.params.Emp_ID
  
    const statement = `delete from EMP_tbl where Emp_ID = ${Emp_ID}`
    db.connnection.query(statement, (error, emps) => {
      // create a result using status and data/error keys
      const result = utils.createResult(error, emps)
      response.send(result)
    })
  })


// // login functionality for Employee
// router.post('/emp/login', (request, response) => {
//   const { Email, Password } = request.body

//   // encrypt the password
  
//   const statement = `select Emp_ID, Name, Email, Password,Mobile,Address,Salary,Designation from EMP_tbl where Email = '${Email}' and Password = '${Password}'`
//   db.connnection.query(statement, (error, users) => {
//     const result = {}

//     if (error)
//      {
//       // error while executing the query
//       result['status'] = 'error'
//       result['error'] = error
//     } 
//     else 
//     {
//       // check if any user has email and passowrd
//       if (users.length == 0) 
//       {
//         // there is no user matching email and password
//         result['status'] = 'error'
//         result['error'] = 'invalid credentials'
//       } 
//       else 
//       {
//         // get the logged in user's profile
//         const user = users[0]

//         // user found with matching email and password
//         result['status'] = 'success'
//         result['data'] = user
//       }
//     }

//     response.send(result)
//   })
// })


//Store password in encrypted format while registration.
router.post('/signup', (request, response) => {
  // turn all the keys from request.body into constants
  const { Name, Email, Password,Mobile,Address,Salary,Designation } = request.body

  // encrypt the password
  const encryptedPassword = crypto.SHA256(Password)

  const statement = `insert into EMP_tbl (Name, Email, Password,Mobile,Address,Salary,Designation) values ('${Name}', '${Email}', '${encryptedPassword}','${Mobile}','${Address}','${Salary}','${Designation}')`
  
  db.connnection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})


// login functionality for Employee using encrpted methode
router.post('/login', (request, response) => {
  const { Email, Password } = request.body

  // encrypt the password
  
  const encryptedPassword= crypto.SHA256(Password)
  const statement = `select Emp_ID, Name, Email, Password,Mobile,Address,Salary,Designation from EMP_tbl where Email = '${Email}' and Password = '${encryptedPassword}'`
  db.connnection.query(statement, (error, users) => {
    const result = {}

    if (error)
     {
      // error while executing the query
      result['status'] = 'error'
      result['error'] = error
    } 
    else 
    {
      // check if any user has email and passowrd
      if (users.length == 0) 
      {
        // there is no user matching email and password
        result['status'] = 'error'
        result['error'] = 'invalid credentials'
      } 
      else 
      {
        // get the logged in user's profile
        const user = users[0]

        // user found with matching email and password
        result['status'] = 'success'
        result['data'] = user
      }
    }

    response.send(result)
  })
})
  

module.exports = router