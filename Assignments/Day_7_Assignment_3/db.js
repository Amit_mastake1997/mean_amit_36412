
const mysql = require('mysql2')

// Create the connection pool. The pool-specific settings are the defaults


const pool = mysql.createPool({
  
  host: 'localhost',
  user: 'root',
  password: 'manager',
  database: 'mystore',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
})

module.exports = {
  connnection: pool
}

            