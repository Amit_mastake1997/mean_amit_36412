


// a) using JSON- 
//* print All Books having price less than 500. 

function function1()
{

const Book =
[

{Book_Name: 'book1', ISBN_No: 1001, Author: 'shraddha',Price: 200,Category: 'A'},
{Book_Name: 'book2', ISBN_No: 1002, Author: 'manali',Price: 2000,Category: 'B'},
{Book_Name: 'book3', ISBN_No: 1003, Author: 'sabiya',Price: 500,Category: 'C'},
{Book_Name: 'book4', ISBN_No: 1004, Author: 'archana',Price: 100,Category: 'E'},
{Book_Name: 'book5', ISBN_No: 1005, Author: 'rubina',Price: 3000,Category: 'D'}
]

const booksLessThan500 = Book.filter(book => {
return book['Price'] < 500
})

console.log(Book)
console.log(booksLessThan500)
}

function1()


console.log('--------------------')


//b)using Object-
//* update some attributes and print Book information.

const Book1 = new Object()


Book1.Book_Name = "book10"
Book1.ISBN_No = 1010
Book1.Author = 'xyz'
Book1.Price= 800
Book1.Category='W'

console.log(Book1)

console.log(`Book_Name: ${Book1.Book_Name}`)
console.log(`ISBN_No: ${Book1.ISBN_No}`)
console.log(`Author: ${Book1.Author}`)
console.log(`Price: ${Book1.Price}`)
console.log(`Category: ${Book1.Category}`)



console.log('--------------------')

const Book2 = new Object()


Book2.Book_Name = "book11"
Book2.ISBN_No = 1011
Book2.Author = 'mnp'
Book2.Price= 5000
Book2.Category='X'
Book2.Pages=1000
Book2.No_of_Copy=10

console.log(Book2)

console.log(`Book_Name: ${Book2.Book_Name}`)
console.log(`ISBN_No: ${Book2.ISBN_No}`)
console.log(`Author: ${Book2.Author}`)
console.log(`Price: ${Book2.Price}`)
console.log(`Category: ${Book2.Category}`)
console.log(`Pages: ${Book2.Pages}`)
console.log(`No Of Copy: ${Book2.No_of_Copy}`)



console.log('--------------------')


//c) using Constructor Function- 
// * print Book Information .


function Book(name,no,author,price,category) {
this.Book_Name = name
this.ISBN_No = no
this.Author = author
this.Price=price
this.Category=category
}

const b1 = new Book('book101',10101,'ABC',500,'T')
console.log(b1)

const b2 = new Book('book102',10102,'PQR',1000,'L')
console.log(b2)

const b3 = new Book('book103',10101,'XYZ',1500,'P')
console.log(b3)

console.log('--------------------')